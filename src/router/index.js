import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */
import componentsRouter from './modules/components'
import chartsRouter from './modules/charts'
import tableRouter from './modules/table'
import nestedRouter from './modules/nested'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: '仪表盘',
        meta: { title: '仪表盘', icon: 'dashboard', affix: true }
      }
    ]
  },
  // ansibleuiRouter,
  {
    path: '/cmdb',
    component: Layout,
    redirect: '/cmdb/host',
    name: 'CMDB',
    meta: {
      title: '资产管理',
      icon: 'table'
    },
    children: [
      {
        path: 'host',
        component: () => import('@/views/ansibleui/host'),
        name: 'host',
        meta: { title: '主机' }
      },
      {
        path: 'group',
        component: () => import('@/views/ansibleui/group'),
        name: 'group',
        meta: { title: '项目组' }
      },
      {
        path: 'user',
        component: () => import('@/views/ansibleui/user'),
        name: 'user',
        meta: { title: '账户' }
      },
      // {
      //   path: 'job',
      //   component: () => import('@/views/ansibleui/job'),
      //   name: 'job',
      //   meta: { title: '任务' }
      // }
    ]
  },
  {
    path: '/ansible',
    component: Layout,
    redirect: '/ansible/index',
    name: 'Ansible',
    meta: {
      title: 'Ansible 任务',
      icon: 'nested'
    },
    children: [
      {
        path: 'exec',
        component: () => import('@/views/ansibleui/index'),
        name: 'exec',
        meta: { title: '任务执行' }
      },
      {
        path: 'playbook',
        component: () => import('@/views/ansibleui/playbook'),
        name: 'playbook',
        meta: { title: '任务剧本' }
      },
      {
        path: 'task',
        component: () => import('@/views/ansibleui/task'),
        name: 'task',
        meta: { title: '执行记录' }
      },
      {
        path: 'task_detail/:id(\\d+)',
        component: () => import('@/views/ansibleui/task_detail'),
        name: 'task_detail',
        meta: { title: '任务详情', noCache: true, },
        hidden: true
      },
    ]
  },
  {
    path: '/celery',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/ansibleui/celery'),
        name: 'Celery',
        meta: { title: 'Celery 管理', icon: 'guide' }
      }
    ]
  },
  {
    path: '/devops',
    component: Layout,
    name: '自动化运维',
    meta: {
      title: '自动化运维',
      icon: 'international'
    },    
    children: [
      {
        path: 'zabbix',
        component: () => import('@/views/ansibleui/zabbix'),
        name: 'Zabbix 监控',
        meta: { title: 'Zabbix 监控', icon: 'skill', affix: true }
      },
      {
        path: 'jenkins',
        component: () => import('@/views/ansibleui/jenkins'),
        name: 'Jenkins 发布',
        meta: { title: 'Jenkins 发布', icon: 'skill', affix: true }
      }
    ]
  },
  {
    path: '/article',
    component: Layout,
    redirect: '/article/list',
    name: '档案室',
    meta: {
      title: '档案室',
      icon: 'documentation'
    },
    children: [
      {
        path: 'create',
        component: () => import('@/views/ansibleui/article_create'),
        // component: () => import('@/views/example/create'),
        name: '撰写资料',
        meta: { title: '撰写资料', icon: 'edit' }
      },
      {
        path: 'edit/:id(\\d+)',
        component: () => import('@/views/example/edit'),
        name: 'EditArticle',
        meta: { title: 'Edit Article', noCache: true, activeMenu: '/example/list' },
        hidden: true
      },
      {
        path: 'list',
        component: () => import('@/views/ansibleui/article_list'),
        name: '资料列表',
        meta: { title: '资料列表', icon: 'list' }
      }
    ]
  },
  {
    path: '/link',
    component: Layout,
    name: '链接',
    meta: {
      title: '链接',
      icon: 'link'
    },
    children: [
      {
        path: 'http://ansibleui.cn/admin/',
        meta: { title: 'Admin 后台',  }
      },
      {
        path: 'http://ansibleui.cn/doc_v1/',
        meta: { title: 'API v1',  }
      },
      {
        path: 'http://ansibleui.cn/doc_v2/',
        meta: { title: 'API v2',  }
      }
    ]
  },
  {
    path: '/documentation',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/ansibleui/document'),
        name: '文章资料',
        meta: { title: '文章资料', icon: 'documentation', affix: true }
      }
    ]
  },
  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        meta: { title: 'Profile', icon: 'user', noCache: true }
      }
    ]
  }

]


const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
