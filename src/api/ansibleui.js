import request from '@/utils/request'


export function fetchArticleList(query) {
  return request({
    url: 'https://miniprogram.ansibleui.cn/api/posts',
    method: 'get',
    params: query
  })
}

export function getApi(path) {
  return request({
    url: 'http://www.ansibleui.cn/'+path+'/',
    method: 'get',
  })
}

export function fetchList(path,query) {
  return request({
    url: 'http://www.ansibleui.cn/api/'+path+'/',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/vue-element-admin/article/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data) {
  return request({
    url: '/vue-element-admin/article/create',
    method: 'post',
    data
  })
}

export function updateArticle(data) {
  return request({
    url: '/vue-element-admin/article/update',
    method: 'post',
    data
  })
}
