import request from '@/utils/request'

export function login(data) {
  console.log('执行 login ')
  return request({
    // url: '/vue-element-admin/user/login',
    url: 'http://www.ansibleui.cn/jwt/login/',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: 'http://www.ansibleui.cn/myself/',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/vue-element-admin/user/logout',
    method: 'post'
  })
}
