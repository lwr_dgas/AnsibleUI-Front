# AnsibleUI-Front

#### 介绍

AnsibleUI 是基于Django + Ansible + Celery 的Web平台，用以批量的任务异步处理

**AnsibleUI-Front 为 AnsibleUI 前后端分离的前端代码**

前端代码地址：https://gitee.com/lwr_dgas/AnsibleUI-Front

后端代码地址：https://gitee.com/lwr_dgas/AnsibleUI

Demo 地址： http://front.ansibleui.cn

#### 软件架构

软件架构说明
*   Ansible使用公私钥登录进行主机操作

![](tmp/a.png)

**该项目在实验楼上有开发教程，地址为 [https://www.shiyanlou.com/courses/1380](https://www.shiyanlou.com/courses/1380)**

**前后端分离 [front.ansibleui.cn](http://front.ansibleui.cn) http://front.ansibleui.cn**

**账号密码 admin:12345678 ， demo 未启动 Celery 进程，请勿添加任务**

**QQ群：929411662，群名称：AnsibleUI**

**群二维码**

![](tmp/qq_qr.png)

**近期内，项目准备重新开发前端页面，使用 AdminLTE 框架**

效果图


![](tmp/001.png)

![](tmp/002.png)

![](tmp/003.png)

![](tmp/004.png)
